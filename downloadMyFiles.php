<?php
/**
 * Plugin to allow participant of survey to dowload all file they uploaded
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.3.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class downloadMyFiles extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Allow participant of survey to dowload all file they uploaded.';
    protected static $name = 'downloadMyFiles';

    /** The settings : update in beforeSurveySettings **/
    protected $settings = array(
        'downloadCurrent' => array(
            'type'=>'info',
            'content'=>'',
        ),
        'downloadLast' => array(
            'type'=>'info',
            'content'=>'',
        ),
        'downloadByToken' => array(
            'type'=>'info',
            'content'=>'',
        ),
        'downloadBySrid' => array(
            'type'=>'info',
            'content'=>'',
        ),
    );

    /**
     * @see getPluginSettings
     */
    public function getPluginSettings($getValues = true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $dowloadUrl = Yii::app()->getController()->createUrl('plugins/direct', array('plugin' => $this->getName()));
        $helpString = sprintf(
            $this->translate("To show to the user a link allowing to download files in current survey : %s."),
            "<code>".$dowloadUrl."</code>"
        );
        $this->settings['downloadCurrent']['content']="<div class='well'>{$helpString}</div>";

        $dowloadUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array('plugin' => $this->getName(), 'surveyid' => 'SID')
        );
        $dowloadUrl = str_replace(array("SID"), array("{SID}"), $dowloadUrl);
        $helpString = sprintf(
            $this->translate("To show to the user a link allowing to download files on last submited (in end page for example) : %s. LimeSurvey replace %s by the survey number"),
            "<code>".$dowloadUrl."</code>",
            "<code>{SID}</code>"
        );
        $this->settings['downloadLast']['content']="<div class='well'>{$helpString}</div>";

        $dowloadUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array('plugin' => $this->getName(), 'surveyid' => 'SID','token'=>'TOKEN')
        );
        $dowloadUrl = str_replace(array("SID","TOKEN"), array("{SID}","{TOKEN}"), $dowloadUrl);
        $helpString = sprintf(
            $this->translate("To show to the user a link allowing to download files with token : %s."),
            "<code>".$dowloadUrl."</code>"
        );
        $this->settings['downloadByToken']['content']="<div class='well'>{$helpString}</div>";

        $dowloadUrl = Yii::app()->getController()->createUrl(
            'plugins/direct',
            array('plugin' => $this->getName(), 'surveyid' => 'SID','srid'=>'SAVEDID')
        );
        $dowloadUrl = str_replace(array("SID","SAVEDID"), array("{SID}","{SAVEDID}"), $dowloadUrl);
        $helpString = sprintf(
            $this->translate("To show to the user a link allowing to download files by response id : %s. It can used by admin or in some situation. Paramaters srid can be used with survey with token too."),
            "<code>".$dowloadUrl."</code>"
        );
        $this->settings['downloadBySrid']['content']="<div class='well'>{$helpString}</div>";

        return parent::getPluginSettings($getValues);
    }

    /** Register to events **/
    public function init()
    {
        $this->subscribe('newDirectRequest', 'download');
        $this->subscribe('beforeSurveyPage');
        /* New attribute */
        $this->subscribe('newQuestionAttributes', 'addDownloadMyFilesAttribute');
    }

    /**
     * Adding the attrib_ute
     */
    public function addDownloadMyFilesAttribute()
    {
        $eventQuestionAttribute = $this->getEvent();
        $DownloadMyFilesAttributes = array(
            'inDownloadMyFiles' => array(
                'name'      => 'inDownloadMyFiles',
                'types'     => '|',
                'category'  => $this->translate('Other'),
                'sortorder' => 900,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Include files in downloadMyFiles zip'),
                'default'   => '1',
                'help'      => $this->translate('You can choose to disable the files from this question in downloadMyFiles zip directory.'),
            ),
        );
        if(method_exists($eventQuestionAttribute,'append')) {
            $eventQuestionAttribute->append('questionAttributes', $DownloadMyFilesAttributes);
        } else {
            $questionAttributes = (array) $this->event->get('questionAttributes');
            $questionAttributes = array_merge($questionAttributes, $DownloadMyFilesAttributes);
            $eventQuestionAttribute->set('questionAttributes', $questionAttributes);
        }
    }

    /**
     * Adding current srid to allowed download allowed
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyid = $this->getEvent()->get('surveyid');
        if (empty($_SESSION['survey_'.$surveyid]['srid'])) {
            return;
        }
        $aSessionAllowedRigth = Yii::app()->session["downloadMyFilesRights"];
        $aSessionAllowedRigth[$surveyid] = $_SESSION['survey_'.$surveyid]['srid'];
        Yii::app()->session["downloadMyFilesRights"] = $aSessionAllowedRigth;
    }
    public function download()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        if ($oEvent->get('target') != get_class()) {
            return;
        }
        $surveyid = Yii::app()->getRequest()->getParam('surveyid', Yii::app()->getRequest()->getParam('sid'));
        $srid = Yii::app()->getRequest()->getParam('srid');
        $token = Yii::app()->getRequest()->getParam('token');

        /* No surveyid : get current one by session ? */
        if (empty($surveyid)) {
            $surveyid = App()->getSession()->get('LEMsid');
        }
        if (empty($surveyid)) {
            throw new CHttpException(400);
        }

        /* Survey id : get it ? */
        $oSurvey=Survey::model()->findByPk($surveyid);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('Invalid survey ID'));
        }
        if ($oSurvey->active != "Y") {
            throw new CHttpException(404, gT('Invalid survey ID'));
        }

        /* Right with token */
        if ($token) {
            // No need to check if token table, just check if anonymous (token column exist in non anonymous)
            if ($oSurvey->anonymized == "Y") {
                throw new CHttpException(400, $this->translate('This survey is anonymized.'));
            }
            if ($srid) {
                // Token and srid : must check if srid is for this token.
                $oResponse = Response::model($surveyid)->findByPk($srid);
                if ($oResponse && $oResponse->token != $token) {
                    throw new CHttpException(401);
                }
            }
        } else {
            /* Right with srid */
            $currentSrid = isset($_SESSION['survey_'.$surveyid]['srid']) ? $_SESSION['survey_'.$surveyid]['srid'] : null;
            $allowedSrid = null;
            $aSessionAllowedRigth = Yii::app()->session["downloadMyFilesRights"];
            if (!empty($aSessionAllowedRigth[$surveyid])) {
                $allowedSrid = $aSessionAllowedRigth[$surveyid];
            }
            if (!empty($srid)) {
                if ( $srid != $currentSrid &&
                    $srid != $allowedSrid &&
                    !Permission::model()->hasSurveyPermission($surveyid, 'reponse', 'read')
                ) {
                    throw new CHttpException(401);
                }
            }
            if (empty($srid)) {
                $srid = empty($currentSrid) ? $allowedSrid : $currentSrid;
            }
            if (empty($srid)) {
                throw new CHttpException(400);
            }
        }

        if (empty($srid)) {
            $criteria = new CDBCriteria;
            $criteria->select = "id";
            $criteria->compare("token", $token);
            $aoResponse = \Response::model($surveyid)->findAll($criteria);
            $aSrid = \CHtml::listData($aoResponse, 'id', 'id');
            $zipfilename = "Files_for_survey_{$surveyid}_response_".Token::sanitizeToken($token).".zip";
        } else {
            $aSrid = array($srid);
            $zipfilename = "Files_for_survey_{$surveyid}_response_".intval($srid).".zip";
        }
        $this->getZipFile($surveyid, $aSrid, $zipfilename);
    }


    /**
     * @param integer $iSurveyID
     * @param integer[] $responseIds
     * @param string $zipfilename
     */
    private function getZipFile($iSurveyID, $responseIds, $zipfilename)
    {
        $uploaddir = App()->getConfig('uploaddir').DIRECTORY_SEPARATOR."surveys".DIRECTORY_SEPARATOR.$iSurveyID.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR;
        $tmpdir = App()->getConfig('tempdir').DIRECTORY_SEPARATOR;

        $filelist = array();
        $responses = Response::model($iSurveyID)->findAllByPk($responseIds);
        $filecount = 0;
        if (floatval(Yii::app()->getConfig('versionnumber')) > 3) {
            $oUploadQuestions = Question::model()->findAll(
                "sid = :sid and type = :type",
                array( ":sid" => $iSurveyID, ":type" => "|")
            );
        } else {
            $language = Survey::model()->findByPk($iSurveyID)->getLanguage();
            $oUploadQuestions = Question::model()->findAll(
                "sid = :sid and type = :type and language = :language",
                array( ":sid" => $iSurveyID, ":type" => "|", ":language" => $language)
            );
        }
        foreach ($responses as $response) {
            foreach($oUploadQuestions as $oUploadQuestion) {
                /* Get the atribute */
                $oQuetsionAttribute = QuestionAttribute::model()->find(
                    "qid = :qid and attribute = :attribute",
                    array(":qid" => $oUploadQuestion->qid, ":attribute" => "inDownloadMyFiles")
                );
                if(empty($oQuetsionAttribute) || $oQuetsionAttribute->value) {
                    foreach ($response->getFiles($oUploadQuestion->qid) as $file) {
                        $filecount++;
                        if (file_exists($uploaddir.basename($file['filename']))) {
                            /*
                            * Now add the file to the archive, prefix files with responseid_index to keep them
                            * unique. This way we can have 234_1_image1.gif, 234_2_image1.gif as it could be
                            * files from a different source with the same name.
                            */
                            $filelist[] = array(
                                $uploaddir.basename($file['filename']),
                                sprintf(
                                    "%05s_%02s_%s",
                                    $response->id,
                                    $filecount,
                                    sanitize_filename(rawurldecode($file['name']))
                                )
                            );
                        }
                    }
                }
            }
        }

        if (count($filelist) > 0) {
            $zip = new ZipArchive(); 
            $zip->open($tmpdir  . DIRECTORY_SEPARATOR . $zipfilename,ZipArchive::CREATE);
            foreach($filelist as $aFile) {
                $zip->addFile($aFile[0],$aFile[1]);
            }
            $zip->close();
            if (file_exists($tmpdir . DIRECTORY_SEPARATOR . $zipfilename)) {
                @ob_clean();
                header('Content-Description: File Transfer');
                header('Content-Type: application/zip, application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($zipfilename));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header("Cache-Control: must-revalidate, no-store, no-cache");
                header('Content-Length: '.filesize($tmpdir  . DIRECTORY_SEPARATOR . $zipfilename));
                readfile($tmpdir  . DIRECTORY_SEPARATOR . $zipfilename);
                unlink($tmpdir  . DIRECTORY_SEPARATOR . $zipfilename);
                exit;
            }
        }
        throw new CHttpException(404, gT('Files not found'));
    }

    /**
    * get translation
    * @param string $string to translate
    * @param string escape mode
    * @param string language, current by default
    * @return string
    */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable(array($this, 'gT'))) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return gT($string, $sEscapeMode, $sLanguage);
    }
}
