# downloadMyFiles

Plugin to allow participant of survey to dowload all file they uploaded.
You find the real URL to be used in Plugin Settings.

This plugin was tested on 3.10 and up. This plugin was not tested on LimeSurvey 6 but must work.

## Usage

URL shown here are for GET url, you have to update replace `/index.php?r=plugins/direct` by `/index.php/plugins/direct` (or `/plugins/direct` with showScriptName to false).

- To show to the user a link allowing to download files in current survey : `/index.php?r=plugins/direct&plugin=downloadMyFiles`
- To show to the user a link allowing to download files on last submited (at end page for example) : `/index.php?r=plugins/direct&plugin=downloadMyFiles&surveyid={SID}`
- To show to the user a link allowing to download files with token : `/index.php?r=plugins/direct&plugin=downloadMyFiles&surveyid={SID}&token={TOKEN}` (all files by this token, can came from different response).
- To show to admin user a link allowing to download files with srid : `/index.php?r=plugins/direct&plugin=downloadMyFiles&surveyid={SID}&srid={SAVEDID}` response (srid) permission is tested again survey start in same session or for admin user.

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright © 2020-2023 Denis Chenu <http://sondages.pro>
- Support <http://support.sondages.pro/>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
